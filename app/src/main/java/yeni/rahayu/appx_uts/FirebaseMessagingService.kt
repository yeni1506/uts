package yeni.rahayu.appx_uts

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat.getSystemService
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class FirebaseMessagingService : FirebaseMessagingService() {

    var body = ""
    var title = ""
    val RC_INTENT = 100
    val CHANNEL_ID = "tes-notif-dan-login"

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)

        val intent = Intent(this, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

        if(p0.notification != null){
            body = p0.notification!!.body!!
            title = p0.notification!!.title!!

            intent.putExtra("title", title)
            intent.putExtra("body", body)
            intent.putExtra("type", 1)

            sendNotif(title, body, intent)
        }
    }
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun sendNotif(title:String, body:String, intent: Intent){
        val pendingIntent = PendingIntent.getActivity(this, RC_INTENT, intent, PendingIntent.FLAG_ONE_SHOT)
        val ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
        val audioAttributes = AudioAttributes.Builder().
        setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE).
        setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION).build()
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val mChannel = NotificationChannel(CHANNEL_ID, "tes-notif-dan-login",
                NotificationManager.IMPORTANCE_HIGH)
            mChannel.description = "tes-notif-dan-login"
            mChannel.setSound(ringtoneUri, audioAttributes)
            notificationManager.createNotificationChannel(mChannel)
        }
        val notificationBuilder = NotificationCompat.Builder(baseContext, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.ic_launcher_foreground))
                .setContentTitle(title)
                .setContentText(body)
                .setSound(ringtoneUri)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true).build()
        notificationManager.notify(RC_INTENT, notificationBuilder)
    }
}