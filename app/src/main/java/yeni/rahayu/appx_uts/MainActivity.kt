package yeni.rahayu.appx_uts

import android.app.Activity
import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.SimpleAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class MainActivity : AppCompatActivity(), View.OnClickListener {

    val COLLECTION = "data_lamaran"
    val F_NAME = "nama"
    val F_ALAMAT = "alamat"
    val F_NO_HP = "no_hp"

    var docId = ""
    lateinit var db : FirebaseFirestore
    lateinit var alData : ArrayList<HashMap<String,Any>>
    lateinit var adapter : SimpleAdapter

    lateinit var filepath : Uri

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        alData = ArrayList()
        btnKirim.setOnClickListener(this)


        btnFile.setOnClickListener(this)
        btnUpload.setOnClickListener(this)

    }

    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnKirim -> {
                val hm = HashMap<String, Any>()
                hm.set(F_NAME, edNama.text.toString())
                hm.set(F_ALAMAT, edAlamat.text.toString())
                hm.set(F_NO_HP, edNohp.text.toString())

                db.collection(COLLECTION).document().set(hm).addOnSuccessListener {
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle("Data berhasil ditambahkan")
                    builder.setMessage("Ingin keluar dari aplikasi?")
                    builder.setPositiveButton("Ya",{ dialog: DialogInterface?, which: Int ->
                        finish()
                    })
                    builder.setNegativeButton("Tidak",{ dialog: DialogInterface?, which: Int ->

                    })
                    builder.show()
                }.addOnFailureListener { e ->
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle("Data gagal ditambahkan")
                    builder.setMessage("Cek kembali data yang anda inputkan?")
                    builder.setPositiveButton("Ya",{ dialog: DialogInterface?, which: Int ->

                    })
                    builder.show()
                }
            }

            R.id.btnFile -> {
                startFileChooser()
            }
            R.id.btnUpload -> {
                uploadFile()
            }
        }
    }

    private fun uploadFile() {
        if(filepath!=null){
            var pd = ProgressDialog(this)
            pd.setTitle("Uploading")
            pd.show()

            var metaCursor = contentResolver.query(filepath, arrayOf(MediaStore.MediaColumns.DISPLAY_NAME),null,null,null)!!
            metaCursor.moveToFirst()
            var fileName = metaCursor.getString(0)
            metaCursor.close()

            var imageRef = FirebaseStorage.getInstance().reference.child(fileName)
            imageRef.putFile(filepath)
                    .addOnSuccessListener {p0 ->
                        pd.dismiss()
                        Toast.makeText(this,"File Uploaded", Toast.LENGTH_SHORT).show()
                    }
                    .addOnFailureListener {p0 ->
                        pd.dismiss()
                        Toast.makeText(applicationContext,p0.message, Toast.LENGTH_SHORT).show()
                    }
                    .addOnProgressListener {p0 ->
                        var progress = (100.0 * p0.bytesTransferred) / p0.totalByteCount
                        pd.setMessage("Uploading ${progress.toInt()}%")
                    }
        }
    }

    private fun startFileChooser() {
        var i = Intent()
        i.setType("*/*")
        i.setAction(Intent.ACTION_GET_CONTENT)
        startActivityForResult(Intent.createChooser(i,"Choose File"),111)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode==111 && resultCode == Activity.RESULT_OK && data != null) {
            filepath = data.data!!
            var bitmap = MediaStore.Images.Media.getBitmap(contentResolver,filepath)

        }
    }
}